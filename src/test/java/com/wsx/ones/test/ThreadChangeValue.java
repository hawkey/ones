package com.wsx.ones.test;

import com.wsx.ones.test.bean.InputParam;
import com.wsx.ones.test.bean.OutputData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by wangshuaixin on 17/6/8.
 */
public class ThreadChangeValue {

    private ThreadChangeValue() {}
    private static final ThreadChangeValue INSTANCE = new ThreadChangeValue();
    public static ThreadChangeValue getInstance() {
        return INSTANCE;
    }

    public OutputData exec(InputParam param) {
        Date date = new Date();

        List<String> list = new ArrayList<String>();


        OutputData data = new OutputData();

        return data;
    }

    public String getData(InputParam param) {
        Date date = new Date();

        List<String> list = new ArrayList<String>();
        double[] arrs = new double[2];

        constructData(param, list, arrs);


        String value = getValue(param, list, arrs);

        return value;
    }

    private String getValue(InputParam param, List<String> list, double[] arrs) {
        StringBuilder build = new StringBuilder();
        build.append(param.getAppId()).append("-")
                .append(param.getName()).append("_")
                .append(list.get(0)).append("==")
                .append(list.get(1)).append("==")
                .append(arrs[0]).append("~").append(arrs[1]);
        return build.toString();
    }

    private void constructData(InputParam param, List<String> list, double[] arrs) {
        switch (param.get_id()) {
            case 1:
                list.add(param.getName() + "-" + 1);
                list.add(param.getName() + "-" + 11);
                arrs[0] = 1.0;
                arrs[1] = 1.1;
                break;
            case 2:
                list.add(param.getName() + "-" + 2);
                list.add(param.getName() + "-" + 22);
                arrs[0] = 2.0;
                arrs[1] = 2.2;
                break;
            case 3:
                list.add(param.getName() + "-" + 3);
                list.add(param.getName() + "-" + 33);
                arrs[0] = 3.0;
                arrs[1] = 3.3;
                break;
            case 4:
                list.add(param.getName() + "-" + 4);
                list.add(param.getName() + "-" + 44);
                arrs[0] = 4.0;
                arrs[1] = 4.4;
                break;
            default:
                list.add(param.getName() + "-" + 0);
                list.add(param.getName() + "-" + 00);
                arrs[0] = 0.0;
                arrs[1] = 0.00;
                break;
        }
    }

    public static void main(String[] args) throws Exception {
        ExecutorService service = Executors.newFixedThreadPool(100);
        CountDownLatch latch = new CountDownLatch(100);

        List<Future<String>> list = new ArrayList<Future<String>>(100);
        for (int i = 0; i < 100; i++) {
            int id = i % 5;
            InputParam param = new InputParam(id, "app"+id, "name" + i, i * 1.0, System.currentTimeMillis());

            Future<String> future = service.submit(new ThreadTest(param, latch));
            list.add(future);
        }
        latch.await();

        for (Future<String> future : list) {
            System.out.println(future.get());
        }
        service.shutdown();
    }
}
