package com.wsx.ones.test;

import com.wsx.ones.test.bean.InputParam;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * Created by wangshuaixin on 17/6/8.
 */
public class ThreadTest implements Callable<String> {

    private InputParam param;
    private CountDownLatch latch;

    public ThreadTest(InputParam param, CountDownLatch latch) {
        this.param = param;
        this.latch = latch;
    }
    @Override
    public String call() throws Exception {
        latch.countDown();
        //ThreadChangeValue.getInstance().exec(param);
        return ThreadChangeValue.getInstance().getData(param);
    }
}
