package com.wsx.ones.test.bean;

import java.io.Serializable;

/**
 * Created by wangshuaixin on 17/6/8.
 */
public class BaseBean implements Serializable {

    private static final long serialVersionUID = -6901443012966802444L;
    private int _id;
    private String appId;

    public BaseBean() {
        super();
    }
    public BaseBean(int _id, String appId) {
        this._id = _id;
        this.appId = appId;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
