package com.wsx.ones.test.bean;

import java.util.Date;

/**
 * Created by wangshuaixin on 17/6/8.
 */
public class OutputData extends BaseBean {

    private static final long serialVersionUID = -832507636122962624L;
    private int status;
    private String msg;
    private String data;
    private Date date;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
