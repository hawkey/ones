package com.wsx.ones.test.bean;

/**
 * Created by wangshuaixin on 17/6/8.
 */
public class InputParam extends BaseBean {

    private static final long serialVersionUID = 6743912002566239810L;

    private String name;
    private double price;
    private long times;

    public InputParam() {
        super();
    }
    public InputParam(int _id, String appId, String name, double price, long times) {
        super(_id, appId);
        this.name = name;
        this.price = price;
        this.times = times;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getTimes() {
        return times;
    }

    public void setTimes(long times) {
        this.times = times;
    }
}
