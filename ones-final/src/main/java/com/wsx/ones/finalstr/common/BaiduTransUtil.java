package com.wsx.ones.finalstr.common;

/**
 * Created by wangshuaixin on 17/1/5.
 */
public final class BaiduTransUtil {

    //需要被翻译的原文
    public static final String SOURCE_CONTENT = "q";

    public static final String FROM_LANGUAGE = "from";

    public static final String TO_LANGUAGE = "to";

    public static final String TRANS_APPID = "appid";

    public static final String TRANS_SALT = "salt";

    public static final String TRANS_SIGN = "sign";
}
