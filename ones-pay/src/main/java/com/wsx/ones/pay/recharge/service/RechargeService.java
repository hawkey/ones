package com.wsx.ones.pay.recharge.service;

import com.wsx.ones.pay.recharge.bo.RechargeBo;
import com.wsx.ones.pay.recharge.model.Recharge;
import com.wsx.ones.pay.recharge.vo.RechargeVo;

/**
 * Created by wangshuaixin on 16/12/8.
 */
public interface RechargeService {

    RechargeVo rechargeAmount(RechargeBo bo);
}
