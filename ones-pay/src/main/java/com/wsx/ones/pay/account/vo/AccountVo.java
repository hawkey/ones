package com.wsx.ones.pay.account.vo;

import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.vo.BaseVo;

/**
 * Created by wangshuaixin on 17/1/6.
 */
public class AccountVo extends BaseVo {

    public AccountVo () {
        super();
    }
    public AccountVo (ReturnStatus returnStatus) {
        super(returnStatus);
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
