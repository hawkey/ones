package com.wsx.ones.pay.account.mapper;

import com.wsx.ones.pay.account.model.PayTest;

/**
 * Created by wangshuaixin on 16/12/14.
 */
public interface AccountMapper {

    int savePay(PayTest payTest);

    int saveTest(PayTest test);
}
