package com.wsx.ones.pay.recharge;

import com.wsx.ones.pay.PayBaseController;
import com.wsx.ones.pay.recharge.bo.RechargeBo;
import com.wsx.ones.pay.recharge.param.RechargeParam;
import com.wsx.ones.pay.recharge.service.RechargeService;
import com.wsx.ones.pay.recharge.vo.RechargeVo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.PojoCopy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by wangshuaixin on 16/12/8.
 */
@RestController
@Scope("prototype")
public class RechargeController extends PayBaseController {

    @Autowired
    private RechargeService rechargeService;


    @RequestMapping(
            value = "/recharge/amount",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData rechargeAmount(RechargeParam rechargeParam, HttpServletRequest request, HttpServletResponse response) {

        if (!checkPayParam(rechargeParam)) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }

        RechargeBo bo = PojoCopy.copy(rechargeParam, RechargeBo.class);

        RechargeVo vo = rechargeService.rechargeAmount(bo);

        return checkVo(vo, OutputData.class);
    }

}
