package com.wsx.ones.pay.rpc.impl;

import com.wsx.ones.pay.rpc.inter.PayInterface;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wangshuaixin on 17/1/6.
 */
@Service
public class PayInterfaceImpl implements PayInterface {
    @Override
    public String hello(String name) {
        return "pay";
    }

    @Override
    public Map<String, String> test(String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("test", "pay");
        return map;
    }
}
