package com.wsx.ones.pay;

import com.wsx.ones.web.model.InputParam;

/**
 * Created by wangshuaixin on 16/12/25.
 */
public class PayParam extends InputParam {

    private static final long serialVersionUID = -410160303580974916L;
    
    private Integer pid;
    private Integer tid;
    private String tname;
    private String pname;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }
}
