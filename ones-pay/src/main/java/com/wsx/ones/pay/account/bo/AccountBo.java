package com.wsx.ones.pay.account.bo;

import com.wsx.ones.web.bo.BaseBo;

/**
 * Created by wangshuaixin on 17/1/6.
 */
public class AccountBo implements BaseBo {

    private static final long serialVersionUID = -1749412029885982361L;

    private Integer pid;
    private Integer tid;
    private String tname;
    private String pname;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }
}
