package com.wsx.ones.pay.recharge.dao.impl;

import com.wsx.ones.pay.recharge.dao.RechargeDao;
import com.wsx.ones.pay.recharge.mapper.RechargeMapper;
import com.wsx.ones.pay.recharge.model.Recharge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * Created by wangshuaixin on 16/12/8.
 */
@Repository
public class RechargeDaoImpl implements RechargeDao {

    @Autowired
    private RechargeMapper rechargeMapper;

    public boolean saveRecharge(Recharge recharge) {

        return rechargeMapper.saveRecharge(recharge);
    }
}
