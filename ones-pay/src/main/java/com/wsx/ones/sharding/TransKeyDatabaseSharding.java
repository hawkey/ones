package com.wsx.ones.sharding;

import com.dangdang.ddframe.rdb.sharding.api.ShardingValue;
import com.dangdang.ddframe.rdb.sharding.api.strategy.database.SingleKeyDatabaseShardingAlgorithm;
import com.wsx.ones.finalstr.common.CommonFinalUtil;
import com.wsx.ones.finalstr.common.ShardingUtil;

import java.util.Collection;

/**
 * 交易型的数据实行分库分表策略，并且该分表可以动态扩展的操作
 * 分库操作是根据用户的分库一样
 * Created by wangshuaixin on 17/1/6.
 */
public class TransKeyDatabaseSharding implements SingleKeyDatabaseShardingAlgorithm<Integer> {

    @Override
    public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        int db_num = shardingValue.getValue() / ShardingUtil.COUNT_PER_DB;
        for (String database : availableTargetNames) {
            int num = Integer.valueOf(database.substring(database.lastIndexOf(CommonFinalUtil.DOWN_SPIT) + 1));
            if (db_num == num) {
                return database;
            }
        }
        return null;
    }

    @Override
    public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        return null;
    }

    @Override
    public Collection<String> doBetweenSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        return null;
    }
}
