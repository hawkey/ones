package com.wsx.ones.zone.content.dao.impl;

import com.wsx.ones.zone.BaseMongoDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.wsx.ones.zone.content.dao.ZoneContentDao;
import com.wsx.ones.zone.content.model.mongo.ZoneContent;


@Repository
public class ZoneContentDaoImpl extends BaseMongoDaoImpl<ZoneContent> implements ZoneContentDao {
	



	public boolean saveZoneContent(ZoneContent zoneContent) {
		//mongoTemplate.save(zoneContent);
		save(zoneContent);
		return true;
	}
	
	public ZoneContent findOne(String id) {
		Criteria criteria = new Criteria();
		criteria.and("dataId").is(id);
		Query query = new Query(criteria);
		return mongoTemplate.findOne(query, ZoneContent.class);
	}

}
