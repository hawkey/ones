package com.wsx.ones.zone;

import com.google.common.base.Strings;
import com.wsx.ones.web.controller.BaseController;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.OperationEnum;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.BaseVo;
import com.wsx.ones.zone.category.param.CategoryParam;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wsx.ones.common.util.DateUtil;
import com.wsx.ones.zone.content.model.mongo.ZoneContent;


@RestController
@RequestMapping("/Zone")
public abstract class ZoneBaseController extends BaseController implements WebController {

	protected boolean checkZoneContent(ZoneParam zoneParam, OperationEnum operationEnum) {

		return true;
	}

	protected boolean checkCate(CategoryParam categoryParam, OperationEnum operationEnum) {
		if (Strings.isNullOrEmpty(categoryParam.getName())) {
			return false;
		}

		return true;
	}

	@Override
	public BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache) {
		return null;
	}

	@Override
	public BaseUser getUserCache(String uid, boolean ifEhcache) {
		return null;
	}


	@Override
	public OutputData checkVo(BaseVo vo, Class<? extends OutputData> clazz) {
		switch (vo.getReturnStatus()) {
			case SUCCESS:
				break;
			case ERROR_UNKNOW:
			case ERROR_SQL:
				return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
			case DATA_EXIST:
				return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_DATA_EXIT);
			default:
				break;
		}
		return PojoCopy.copy(vo, clazz);
	}
}
