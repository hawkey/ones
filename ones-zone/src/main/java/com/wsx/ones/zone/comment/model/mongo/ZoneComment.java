package com.wsx.ones.zone.comment.model.mongo;

import com.wsx.ones.zone.ZoneBaseBean;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by wangshuaixin on 16/12/11.
 */

@Document(collection = "zone_comment")
public class ZoneComment extends ZoneBaseBean {

    private static final long serialVersionUID = -4118745404152636886L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

}
