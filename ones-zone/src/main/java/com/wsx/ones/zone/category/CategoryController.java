package com.wsx.ones.zone.category;

import com.wsx.ones.common.util.DateUtil;
import com.wsx.ones.web.bo.PageBo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.model.PageData;
import com.wsx.ones.web.model.PageParam;
import com.wsx.ones.web.util.OperationEnum;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.PageVo;
import com.wsx.ones.zone.ZoneBaseController;
import com.wsx.ones.zone.ZoneData;
import com.wsx.ones.zone.ZoneVo;
import com.wsx.ones.zone.category.bo.CateSearchBo;
import com.wsx.ones.zone.category.bo.CategoryBo;
import com.wsx.ones.zone.category.param.CateSearchParam;
import com.wsx.ones.zone.category.param.CategoryParam;
import com.wsx.ones.zone.category.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

/**
 * Created by wangshuaixin on 16/12/29.
 */
@RestController
@Scope("prototype")
public class CategoryController extends ZoneBaseController {

    private static final Logger log = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryService categoryService;


    @RequestMapping(
            value = "/cate/add",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData saveCate(CategoryParam categoryParam) {
        log.info("");
        if (!checkCate(categoryParam, OperationEnum.ADD)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
        }

        CategoryBo categoryBo = PojoCopy.copy(categoryParam, CategoryBo.class);
        categoryBo.setAtime(DateUtil.getDate());

        //
        ZoneVo zoneVo = categoryService.saveCate(categoryBo);

        /**
        switch (zoneVo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_UNKNOW:
            case ERROR_SQL:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            case DATA_EXIST:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_DATA_EXIT);
            default:
                break;
        }

        return PojoCopy.copy(zoneVo, ZoneData.class);
         */
        return checkVo(zoneVo, ZoneData.class);
    }

    @RequestMapping(
            value = "/cate/search",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData getSearchCates(CateSearchParam cateSearchParam) {

        if (!checkPageParam(cateSearchParam)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_TOKEN_ERROR);
        }

        CateSearchBo bo = PojoCopy.copy(cateSearchParam, CateSearchBo.class);

        PageVo vo = categoryService.getSearchCates(bo);
        switch (vo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_UNKNOW:
            case ERROR_SQL:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            default:
                break;
        }

        return PojoCopy.copy(vo, PageData.class);
    }

}
