package com.wsx.ones.zone.category;

import com.wsx.ones.web.bo.PageBo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.model.PageData;
import com.wsx.ones.web.model.PageParam;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.PageVo;
import com.wsx.ones.zone.ZoneBaseController;
import com.wsx.ones.zone.category.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 17/1/16.
 */
@RestController
@Scope("prototype")
public class CategorySearchController extends ZoneBaseController {

    private static final Logger log = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(
            value = "/cate/catch",
            method = {RequestMethod.POST}
    )
    public @ResponseBody
    OutputData getAllCates(PageParam pageParam) {
        log.info("enter");
        if (!checkPageParam(pageParam)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_TOKEN_ERROR);
        }

        //
        PageBo pageBo = PojoCopy.copy(pageParam, PageBo.class);
        //
        PageVo pageVo = categoryService.getAllCates(pageBo);

        return checkVo(pageVo, PageData.class);
    }
}
