package com.wsx.ones.zone.comment.dao.impl;

import com.wsx.ones.zone.BaseMongoDaoImpl;
import com.wsx.ones.zone.comment.dao.ZoneCommentDao;
import com.wsx.ones.zone.comment.model.mongo.ZoneComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by wangshuaixin on 16/12/11.
 */
@Repository
public class ZoneCommentDaoImpl extends BaseMongoDaoImpl<ZoneComment> implements ZoneCommentDao {

    public void saveComment(ZoneComment zoneComment) {
        //mongoTemplate.save(zoneComment);
        save(zoneComment);
    }
}
