package com.wsx.ones.zone;

import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 * Created by wangshuaixin on 17/6/7.
 */
public interface BaseMongoDao<T> {

    T save(T t);

    T save(T t, String collectionName);

    T findById(String id);

    T findById(String id, String collectionName);

    List<T> finds(Query query);

    void remove(T t);

    void remove(T t, String collectionName);

    void remove(Query query);
}
