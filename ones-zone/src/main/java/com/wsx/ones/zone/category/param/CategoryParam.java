package com.wsx.ones.zone.category.param;

import com.wsx.ones.zone.ZoneParam;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public class CategoryParam extends ZoneParam {

    private static final long serialVersionUID = 7994374070487121793L;

    private String name;
    private Integer sindex;
    private Date atime;
    private Long uid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSindex() {
        return sindex;
    }

    public void setSindex(Integer sindex) {
        this.sindex = sindex;
    }

    public Date getAtime() {
        return atime;
    }

    public void setAtime(Date atime) {
        this.atime = atime;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
