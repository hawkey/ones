package com.wsx.ones.zone.category.bo;

import com.wsx.ones.web.bo.BaseBo;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public class CategoryBo implements BaseBo {

    private static final long serialVersionUID = 5330059846113833281L;

    private String _id;
    private String name;
    private Integer sindex;
    private Date atime;
    private Long uid;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSindex() {
        return sindex;
    }

    public void setSindex(Integer sindex) {
        this.sindex = sindex;
    }

    public Date getAtime() {
        return atime;
    }

    public void setAtime(Date atime) {
        this.atime = atime;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
