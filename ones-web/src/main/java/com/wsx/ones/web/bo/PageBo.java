package com.wsx.ones.web.bo;

import com.wsx.ones.web.BoVo;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public class PageBo implements BoVo {

    private static final long serialVersionUID = -3361288364367153315L;

    private Integer pageNo;
    private Integer pageSize;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
