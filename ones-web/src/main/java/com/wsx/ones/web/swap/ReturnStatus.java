package com.wsx.ones.web.swap;

/**
 * Created by wangshuaixin on 16/12/22.
 */
public enum ReturnStatus {

    /**成功*/
    SUCCESS,
    /**未知错误*/
    ERROR_UNKNOW,
    /**sql错误*/
    ERROR_SQL,
    /**不存在*/
    NOT_EXIST,
    /**数据存在*/
    DATA_EXIST
}
