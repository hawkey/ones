package com.wsx.ones.web.model;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public class PageParam extends InputParam {

    private static final long serialVersionUID = -3252262402568573533L;

    private Integer pageNo = 1;
    private Integer pageSize = 10;
    private String orderCol;
    private Integer ordertype;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}
