package com.wsx.ones.web.model;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public class PageData extends OutputData {

    private static final long serialVersionUID = -6811956025898173732L;

    public PageData () {
        super();
    }
    public PageData(int code, int status) {
        super(code, status);
    }

    private List<Map<String, String>> results;

    public List<Map<String, String>> getResults() {
        return results;
    }

    public void setResults(List<Map<String, String>> results) {
        this.results = results;
    }

    private List<Map<String, Object>> rtnObjs;

    public List<Map<String, Object>> getRtnObjs() {
        return rtnObjs;
    }

    public void setRtnObjs(List<Map<String, Object>> rtnObjs) {
        this.rtnObjs = rtnObjs;
    }
}
