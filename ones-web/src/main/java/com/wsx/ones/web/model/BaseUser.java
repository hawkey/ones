package com.wsx.ones.web.model;

import com.wsx.ones.web.BoVo;


/**
 * Created by wangshuaixin on 16/12/19.
 */
public class BaseUser implements BoVo {

    private static final long serialVersionUID = 2792806322868572290L;

    private String token;

    private Long uid;
    private String lname;
    private String nname;
    private String phone;
    private String email;
    private String avatar;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getNname() {
        return nname;
    }

    public void setNname(String nname) {
        this.nname = nname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
