package com.wsx.ones.web;

import java.io.Serializable;

/**
 * 所有在web中各层数据传递的超类接口，后期如果更换序列化容易
 * Created by wangshuaixin on 16/12/23.
 */
public interface BoVo extends Serializable {
}
