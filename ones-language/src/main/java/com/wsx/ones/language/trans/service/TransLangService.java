package com.wsx.ones.language.trans.service;

import com.wsx.ones.language.trans.bo.TransLangBo;
import com.wsx.ones.language.trans.vo.TransLangVo;

public interface TransLangService {

    TransLangVo trans(TransLangBo bo);
}
