package com.wsx.ones.language.trans;

import com.wsx.ones.language.LanguageData;
import com.wsx.ones.language.LanguageParam;
import com.wsx.ones.language.trans.bo.TransLangBo;
import com.wsx.ones.language.trans.service.TransLangService;
import com.wsx.ones.language.trans.vo.TransLangVo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.PojoCopy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

import com.wsx.ones.language.LanguageBaseController;


@RestController
@Scope("prototype")
public class TransLangController extends LanguageBaseController {
	
	private static final Logger log = LoggerFactory.getLogger(TransLangController.class);

	@Autowired
	private TransLangService transLangService;
	
	@RequestMapping(
			value = "/trans/en",
			method = {RequestMethod.POST}
	)
	public @ResponseBody OutputData transLanguage(LanguageParam languageParam) {
		log.info("enter");

		if (!checkTransParam(languageParam)) {
			return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
		}

		TransLangBo bo = PojoCopy.copy(languageParam, TransLangBo.class);

		TransLangVo vo = transLangService.trans(bo);

		/**
		switch (vo.getReturnStatus()) {
			case SUCCESS:
				break;
			case ERROR_UNKNOW:
				return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
			case NOT_EXIST:
				return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_DATA_NOEXIT);
			default:
				break;
		}

		return PojoCopy.copy(vo, LanguageData.class);
		 */
		return checkVo(vo, LanguageData.class);
	}


}
