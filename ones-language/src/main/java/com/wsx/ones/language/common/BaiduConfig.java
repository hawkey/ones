package com.wsx.ones.language.common;

/**
 * Created by wangshuaixin on 17/1/5.
 */
public class BaiduConfig {

    private static String charset;
    private static String appId;
    private static String token;
    private static String url;
    private static String salt;

    public static String getCharset() {
        return charset;
    }

    public static void setCharset(String charset) {
        BaiduConfig.charset = charset;
    }

    public static String getAppId() {
        return appId;
    }

    public static void setAppId(String appId) {
        BaiduConfig.appId = appId;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        BaiduConfig.token = token;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        BaiduConfig.url = url;
    }

    public static String getSalt() {
        return salt;
    }

    public static void setSalt(String salt) {
        BaiduConfig.salt = salt;
    }
}
