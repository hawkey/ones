package com.wsx.ones.language;

import com.wsx.ones.web.controller.BaseController;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.BaseVo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/Language")
public abstract class LanguageBaseController extends BaseController implements WebController {

    protected boolean checkTransParam(LanguageParam languageParam) {

        return true;
    }

    @Override
    public BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache) {
        return null;
    }

    @Override
    public BaseUser getUserCache(String uid, boolean ifEhcache) {
        return null;
    }

    @Override
    public OutputData checkVo(BaseVo vo, Class<? extends OutputData> clazz) {
        switch (vo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_UNKNOW:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            case NOT_EXIST:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_DATA_NOEXIT);
            default:
                break;
        }
        return PojoCopy.copy(vo, clazz);
    }
}
