package com.wsx.ones.ad.index;

import com.wsx.ones.ad.index.param.IndexAdParam;
import com.wsx.ones.ad.index.service.IndexAdService;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.model.PageData;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.BaseVo;
import com.wsx.ones.web.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wsx.ones.ad.AdBaseController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Scope("prototype")
public class IndexAdController extends AdBaseController {

    @Autowired
    private IndexAdService indexAdService;

    @RequestMapping(
            value = "/page/index",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData pageHeader(IndexAdParam indexAdParam, HttpServletRequest request, HttpServletResponse response) {

        if (!checkAd(indexAdParam)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT,WebController.STATUS_ERROR_CHECKCODE);
        }

        PageVo pageVo = indexAdService.pageHeader(indexAdParam.getAdEnum());

        /**
        switch (pageVo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_SQL:
            case ERROR_UNKNOW:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            default:
                break;
        }

        return PojoCopy.copy(pageVo, PageData.class);
         */
        return checkVo(pageVo, PageData.class);
    }

    @Override
    public void doSuccessVo(BaseVo baseVo) {
        System.out.println("success");
    }

    @Override
    public void doErrorVo(BaseVo baseVo) {
        System.out.println("error");
    }
}
