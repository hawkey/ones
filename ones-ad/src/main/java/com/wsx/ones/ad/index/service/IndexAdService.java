package com.wsx.ones.ad.index.service;

import com.wsx.ones.ad.AdEnum;
import com.wsx.ones.ad.index.model.Ad;
import com.wsx.ones.web.vo.PageVo;

/**
 * Created by wangshuaixin on 16/12/9.
 */
public interface IndexAdService {

    PageVo pageHeader(AdEnum adEnum);
}
