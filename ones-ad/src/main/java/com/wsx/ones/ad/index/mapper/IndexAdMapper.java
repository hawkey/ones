package com.wsx.ones.ad.index.mapper;

import com.wsx.ones.ad.index.model.Ad;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/9.
 */
public interface IndexAdMapper {

    int saveAd(Ad ad);

    List<Map<String,Object>> getPageHeaderAd();
}
