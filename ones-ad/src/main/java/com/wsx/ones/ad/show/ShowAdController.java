package com.wsx.ones.ad.show;

import com.wsx.ones.ad.AdBaseController;
import com.wsx.ones.ad.show.service.ShowAdService;
import com.wsx.ones.web.model.OutputData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 17/1/3.
 */
@RestController
@Scope("prototype")
public class ShowAdController extends AdBaseController {

    @Autowired
    private ShowAdService showAdService;

}
