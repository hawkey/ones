package com.wsx.ones.ad.index.service.impl;

import com.wsx.ones.ad.AdEnum;
import com.wsx.ones.ad.index.dao.IndexAdDao;
import com.wsx.ones.ad.index.model.Ad;
import com.wsx.ones.ad.index.service.IndexAdService;
import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/9.
 */
@Service
@Transactional
public class IndexAdServiceImpl implements IndexAdService {

    @Autowired
    private IndexAdDao indexAdDao;

    @Override
    public PageVo pageHeader(AdEnum adEnum) {
        List<Map<String, Object>> list = null;
        switch (adEnum) {
            case INDEX_HEAD:
                list = indexAdDao.getPageHeaderAd();
                break;
            default:
                break;
        }
        if (null == list) {
            return new PageVo(ReturnStatus.ERROR_UNKNOW);
        }
        if (list.size() <= 0) {
            return new PageVo(ReturnStatus.NOT_EXIST);
        }
        PageVo pageVo = new PageVo();
        pageVo.setRtnObjs(list);

        return pageVo;
    }
}
