package com.wsx.ones.ad.rpc.impl;

import com.wsx.ones.ad.rpc.inter.AdInterface;
import org.springframework.stereotype.Service;

/**
 * Created by wangshuaixin on 16/12/9.
 */
@Service
public class AdInterfaceImpl implements AdInterface {

    public String hello(String name) {
        return "HELLO" + name;
    }
}
