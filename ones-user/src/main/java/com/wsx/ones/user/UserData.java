package com.wsx.ones.user;

import com.wsx.ones.web.model.OutputData;

public class UserData extends OutputData {

	private static final long serialVersionUID = 5912643633331737649L;
	private String token;
	private Long stamp;
	
	public UserData() {
		super();
	}

	public UserData(int code, int status) {
		super(code, status);
	}
	
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getStamp() {
		return stamp;
	}

	public void setStamp(Long stamp) {
		this.stamp = stamp;
	}
}
