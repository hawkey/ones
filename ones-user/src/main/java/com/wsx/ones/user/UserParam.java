package com.wsx.ones.user;

import com.wsx.ones.web.model.InputParam;

/**
 * 用户抽象字段-用户模块公用
 * Created by wangshuaixin on 16/12/19.
 */
public class UserParam extends InputParam {

    private static final long serialVersionUID = -1974152372358804384L;

    private String lname;
    private String phone;
    private String email;
    private String passwd;
    private Integer utype;
    //验证码
    private String check;

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Integer getUtype() {
        return utype;
    }

    public void setUtype(Integer utype) {
        this.utype = utype;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }
}
