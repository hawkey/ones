package com.wsx.ones.user.login;

import com.wsx.ones.web.model.BaseUser;
import com.wsx.ones.web.model.OutputData;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wsx.ones.user.UserBaseController;

@RestController
@Scope("prototype")
public class UserLoginController extends UserBaseController {
	
	@RequestMapping(
			value = "/user/login/test/{name}",
			method = {RequestMethod.GET}
	)
	public @ResponseBody OutputData test(@PathVariable String name) {
		BaseUser user = new BaseUser();
		user.setLname("123");

		BaseUser baseUser = getUserCache(name, true);
		if (null == baseUser) {
			setUserCache(user, true);
		}
		return new OutputData();
	}


}
