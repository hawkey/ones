package com.wsx.ones.user.settings;

import com.wsx.ones.user.UserBaseController;
import com.wsx.ones.user.UserLoginEnum;
import com.wsx.ones.user.settings.param.SettingsParam;
import com.wsx.ones.user.settings.service.UserSettingsService;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/13.
 */
@RestController
@Scope("prototype")
public class UserSettingsController extends UserBaseController {

    @Autowired
    private UserSettingsService userSettingsService;

    @RequestMapping(
            value = "/settings/check/phone",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData checkUserPhone(SettingsParam settingsParam) {

        if (!checkUserParam(settingsParam, UserLoginEnum.PHONE)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
        }

        if (checkExistUser(settingsParam, UserLoginEnum.PHONE)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_DATA_EXIT);
        }

        return new OutputData();
    }


}
