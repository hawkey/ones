package com.wsx.ones.user.model;

import com.wsx.ones.user.UserBaseBean;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/19.
 */
public class UserExt1 extends UserBaseBean {

    private static final long serialVersionUID = 8149023682293005386L;

    //private Long uid;
    private String rip;
    private Date rtime;
    private String rapp;
    private String rname;

    //分库
    //private Integer utype;

    public String getRip() {
        return rip;
    }

    public void setRip(String rip) {
        this.rip = rip;
    }

    public Date getRtime() {
        return rtime;
    }

    public void setRtime(Date rtime) {
        this.rtime = rtime;
    }

    public String getRapp() {
        return rapp;
    }

    public void setRapp(String rapp) {
        this.rapp = rapp;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

}
