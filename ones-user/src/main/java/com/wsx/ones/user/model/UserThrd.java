package com.wsx.ones.user.model;

import com.sun.org.apache.xpath.internal.operations.String;
import com.wsx.ones.user.UserBaseBean;

/**
 * Created by wangshuaixin on 16/12/19.
 */
public class UserThrd extends UserBaseBean {

    private static final long serialVersionUID = 42694405852364297L;

    private Long tid;
    //private Long uid;
    private Integer apptype;
    private String appid;
    private String name;
    private String token;
    private String credential;

    //分库
    //private Integer utype;

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Integer getApptype() {
        return apptype;
    }

    public void setApptype(Integer apptype) {
        this.apptype = apptype;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

}
