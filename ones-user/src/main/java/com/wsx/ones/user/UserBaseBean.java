package com.wsx.ones.user;

import com.wsx.ones.core.model.BaseBean;

/**
 * bean的处理主要涉及service调用dao，以及dao调用数据库连接
 */
public class UserBaseBean extends BaseBean {

	private static final long serialVersionUID = 3590142830504197822L;

	private Long uid;
	private Integer utype;

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public Integer getUtype() {
		return utype;
	}

	public void setUtype(Integer utype) {
		this.utype = utype;
	}
}
