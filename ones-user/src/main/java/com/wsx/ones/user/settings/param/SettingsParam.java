package com.wsx.ones.user.settings.param;

import com.wsx.ones.user.UserParam;
import com.wsx.ones.user.settings.SettingsBaseParam;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/28.
 */
public class SettingsParam extends SettingsBaseParam {

    private static final long serialVersionUID = -974347102500104890L;

    private Integer country;
    private Integer province;
    private Integer city;
    private String street;
    private String address;
    private String rlname;
    private Integer idtype;
    private String idnum;
    private Integer sex;
    private Integer nation;
    private Date birthday;
    private String secdemail;
    private String memo;

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRlname() {
        return rlname;
    }

    public void setRlname(String rlname) {
        this.rlname = rlname;
    }

    public Integer getIdtype() {
        return idtype;
    }

    public void setIdtype(Integer idtype) {
        this.idtype = idtype;
    }

    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getNation() {
        return nation;
    }

    public void setNation(Integer nation) {
        this.nation = nation;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSecdemail() {
        return secdemail;
    }

    public void setSecdemail(String secdemail) {
        this.secdemail = secdemail;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
