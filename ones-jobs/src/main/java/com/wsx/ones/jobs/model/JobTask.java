package com.wsx.ones.jobs.model;

import java.io.Serializable;

/**
 * Created by wangshuaixin on 17/1/21.
 */
public class JobTask implements Serializable {

    private static final long serialVersionUID = -3168852565552849479L;

    private String code;//任务的编码，要求唯一
    private String name;//任务的名称
    private String clazz;//任务的执行类
    private String cron;//任务的执行时间表达式

    private Integer taskStatus;//任务的执行状态
    private Integer status;//任务是否开启
    private String memo;//任务的备注

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public Integer getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Integer taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
