package com.wsx.ones.core.secure;

/**
 * Created by wangshuaixin on 16/12/27.
 */
public class CryptoUtil {


    /**
     * 密码解密
     * 主要作用，前后端数据的传输，对于敏感数据需要进行加密解密处理
     * 后期处理，对于加密的key是动态生成后传输到前端，在缓存中保存并验证
     * @param source
     * @return
     */
    public static String decrypt(String source) {

        return source;
    }

    /**
     * 密码加密
     * @param source
     * @return
     */
    public static String encrypt(String source) {
        return MD5Util.getMD5CodeSalt(source, MD5Util.SALT);
    }
}
