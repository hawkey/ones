import com.wsx.ones.mq.util.ExchangeEnum;

/**
 * Created by wangshuaixin on 17/1/22.
 */
public class Test {

    public static void main(String[] args) {
        String value = ExchangeEnum.DIRECT.getValue();

        System.out.println(value);

        switch (ExchangeEnum.FANOUT) {
            case DIRECT:
                System.out.print(ExchangeEnum.DIRECT.getValue());
                break;
            case TOPIC:
                System.out.print(ExchangeEnum.TOPIC.getValue());
                break;
            case HEADERS:
                System.out.print(ExchangeEnum.HEADERS.getValue());
                break;
            case FANOUT:
                System.out.print(ExchangeEnum.FANOUT.getValue());
                break;
            default:
                break;
        }
    }
}
