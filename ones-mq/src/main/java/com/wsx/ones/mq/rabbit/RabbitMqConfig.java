package com.wsx.ones.mq.rabbit;

import java.io.Serializable;

/**
 * Created by wangshuaixin on 17/1/22.
 */
public class RabbitMqConfig implements Serializable {

    private static final long serialVersionUID = -4487517550402903106L;

    private String host;
    private Integer port;
    private String username;
    private String password;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
