package com.wsx.ones.mq.util;

/**
 * Created by wangshuaixin on 17/1/22.
 */
public enum ExchangeEnum {

    DIRECT("direct"),TOPIC("topic"),HEADERS("headers"),FANOUT("fanout");

    private String exchange;

    ExchangeEnum(String exchange) {
        this.exchange = exchange;
    }

    public String getValue() {
        return exchange;
    }

}
