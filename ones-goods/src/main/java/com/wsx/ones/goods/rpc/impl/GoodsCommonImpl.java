package com.wsx.ones.goods.rpc.impl;

import com.wsx.ones.goods.rpc.model.Goods;
import org.springframework.stereotype.Service;

import com.wsx.ones.goods.rpc.inter.GoodsCommonInterface;


@Service
public class GoodsCommonImpl implements GoodsCommonInterface {


	public String get() {
		// TODO Auto-generated method stub
		return "goods";
	}

	public Goods take(String name) {
		Goods goods = new Goods();
		goods.setName(name);
		goods.setPrice(3.14);
		return goods;
	}
}
