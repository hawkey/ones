package com.wsx.ones.search;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/8.
 */
public class SearchBaseBean extends BaseBean {

    private static final long serialVersionUID = 1299975421452346791L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
