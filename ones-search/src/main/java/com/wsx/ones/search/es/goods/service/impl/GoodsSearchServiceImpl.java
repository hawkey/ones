package com.wsx.ones.search.es.goods.service.impl;

import com.wsx.ones.search.SearchBaseBean;
import com.wsx.ones.search.SearchParam;
import com.wsx.ones.search.es.goods.service.GoodsSearchService;
import com.wsx.ones.search.util.ElasticSearchUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/8.
 */
@Service
public class GoodsSearchServiceImpl implements GoodsSearchService {

    /**
     *
     * @param searchParam
     * @return
     */
    public List<Map<String, Object>> searchGoodsByName(SearchParam searchParam) {
        return ElasticSearchUtil.getPageDocumentByName("goods","goods",searchParam.getName(),searchParam.getFrom(),searchParam.getSize());
    }
}
