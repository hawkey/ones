package com.wsx.ones.search.es.goods.service;

import com.wsx.ones.search.SearchBaseBean;
import com.wsx.ones.search.SearchParam;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/8.
 */
public interface GoodsSearchService {

    List<Map<String,Object>> searchGoodsByName(SearchParam searchParam);
}
