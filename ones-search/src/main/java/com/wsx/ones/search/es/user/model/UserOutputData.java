package com.wsx.ones.search.es.user.model;

import com.wsx.ones.web.model.OutputData;

public class UserOutputData extends OutputData {

	private static final long serialVersionUID = 8265128532231909957L;
	
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
