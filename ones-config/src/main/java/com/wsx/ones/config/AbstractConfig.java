package com.wsx.ones.config;

import com.wsx.ones.config.model.ConfigBean;

import java.util.List;
import java.util.Map;

/**
 * 读取配置文件的抽象公用方法类
 * Created by wangshuaixin on 17/3/27.
 */
public abstract class AbstractConfig {


    public abstract Map<String, List<ConfigBean>> getConfigureContent() ;
}
