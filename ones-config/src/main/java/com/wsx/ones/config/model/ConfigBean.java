package com.wsx.ones.config.model;

/**
 * 配置文件的公用bean属性
 * Created by wangshuaixin on 17/3/27.
 */
public class ConfigBean {

    private String id;
    private String name;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
