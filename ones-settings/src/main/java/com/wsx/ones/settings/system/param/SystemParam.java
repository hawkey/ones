package com.wsx.ones.settings.system.param;

import com.wsx.ones.settings.SettingsParam;

/**
 * Created by wangshuaixin on 17/1/8.
 */
public class SystemParam extends SettingsParam {

    private String test;

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
