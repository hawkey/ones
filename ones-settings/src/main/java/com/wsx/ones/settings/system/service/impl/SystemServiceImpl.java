package com.wsx.ones.settings.system.service.impl;

import com.wsx.ones.settings.system.bo.SystemBo;
import com.wsx.ones.settings.system.dao.SystemDao;
import com.wsx.ones.settings.system.model.System;
import com.wsx.ones.settings.system.service.SystemService;
import com.wsx.ones.settings.system.vo.SystemVo;
import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.util.PojoCopy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/11.
 */
@Service
@Transactional
public class SystemServiceImpl implements SystemService {

    @Autowired
    private SystemDao systemDao;

    @Transactional(propagation = Propagation.REQUIRED)
    public SystemVo saveTest(SystemBo bo) {
        System system = PojoCopy.copy(bo, System.class);
        int count = systemDao.saveTest(system);
        if (count <= 0) {
            return new SystemVo(ReturnStatus.ERROR_UNKNOW);
        }
        SystemVo vo = new SystemVo();
        vo.setId(bo.getId());
        return vo;
    }
}
