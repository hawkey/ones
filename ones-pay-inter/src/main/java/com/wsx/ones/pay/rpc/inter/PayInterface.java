package com.wsx.ones.pay.rpc.inter;

import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/8.
 */
public interface PayInterface {

    public String hello(String name);

    public Map<String, String> test(String token);
}
