package com.wsx.ones.user.rpc.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/8.
 */

public class TokenUser implements Serializable {

    private static final long serialVersionUID = 3187124672447057928L;

    private String name;
    private Date atime;
    private Integer num;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAtime() {
        return atime;
    }

    public void setAtime(Date atime) {
        this.atime = atime;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
