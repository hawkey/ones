package com.wsx.ones.sharding;

import com.dangdang.ddframe.rdb.sharding.api.ShardingValue;
import com.dangdang.ddframe.rdb.sharding.api.strategy.database.SingleKeyDatabaseShardingAlgorithm;
import com.wsx.ones.finalstr.common.CommonFinalUtil;

import java.util.Collection;

/**
 * 数据库分库算法实现，主要针对用户会员的算法，前期设计的用户是个人和企业是分开的，vip的特点一样
 * Created by wangshuaixin on 17/1/9.
 */
public class VipDatabaseSharding implements SingleKeyDatabaseShardingAlgorithm<Integer> {

    public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        for (String database : availableTargetNames) {
            if (database.endsWith(shardingValue.getValue() + CommonFinalUtil.STRING_EMPTY)) {
                return database;
            }
        }
        throw new IllegalArgumentException();
    }

    public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        return null;
    }

    public Collection<String> doBetweenSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        return null;
    }
}
