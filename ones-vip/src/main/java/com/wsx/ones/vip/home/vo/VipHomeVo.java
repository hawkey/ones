package com.wsx.ones.vip.home.vo;

import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.vo.BaseVo;

/**
 * Created by wangshuaixin on 17/1/9.
 */
public class VipHomeVo extends BaseVo {

    public VipHomeVo() {
        super();
    }
    public VipHomeVo (ReturnStatus returnStatus) {
        super(returnStatus);
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
