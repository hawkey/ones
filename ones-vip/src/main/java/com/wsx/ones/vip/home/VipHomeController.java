package com.wsx.ones.vip.home;

import com.wsx.ones.vip.VipBaseController;
import com.wsx.ones.vip.VipData;
import com.wsx.ones.vip.home.bo.VipHomeBo;
import com.wsx.ones.vip.home.param.VipHomeParam;
import com.wsx.ones.vip.home.service.VipHomeService;
import com.wsx.ones.vip.home.vo.VipHomeVo;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.PojoCopy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/17.
 */
@RestController
@Scope("prototype")
public class VipHomeController extends VipBaseController {

    private static final Logger log = LoggerFactory.getLogger(VipHomeController.class);

    @Autowired
    private VipHomeService vipHomeService;


    @RequestMapping(
            value = "/home/page",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData getHomePage(VipHomeParam vipHomeParam) {
        log.info("enter");

        if (!checkVipParam(vipHomeParam)) {
            return new OutputData(WebController.CODE_ERROR_CLIENT, WebController.STATUS_ERROR_PARAM);
        }

        VipHomeBo bo = PojoCopy.copy(vipHomeParam, VipHomeBo.class);

        VipHomeVo vo = vipHomeService.saveVip(bo);

        /**
        switch (vo.getReturnStatus()) {
            case SUCCESS:
                break;
            case ERROR_UNKNOW:
                return new OutputData(WebController.CODE_ERROR_SERVER, WebController.STATUS_ERROR_UNKNOWN);
            default:
                break;
        }


        return PojoCopy.copy(vo, VipData.class);
         */
        return checkVo(vo, VipData.class);
    }
}
