package com.wsx.ones.vip.home.bo;

import com.wsx.ones.web.bo.BaseBo;

/**
 * Created by wangshuaixin on 17/1/9.
 */
public class VipHomeBo implements BaseBo {

    private static final long serialVersionUID = -2152626175221811673L;

    private Integer id;
    private Integer vtype;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVtype() {
        return vtype;
    }

    public void setVtype(Integer vtype) {
        this.vtype = vtype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
