package com.wsx.ones.vip.home.mapper;

import com.wsx.ones.vip.home.model.VipHome;

/**
 * Created by wangshuaixin on 17/1/9.
 */
public interface VipHomeMapper {

    int saveVip(VipHome home);
}
