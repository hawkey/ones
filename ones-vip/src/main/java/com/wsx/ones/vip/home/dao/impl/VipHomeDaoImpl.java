package com.wsx.ones.vip.home.dao.impl;

import com.wsx.ones.vip.home.dao.VipHomeDao;
import com.wsx.ones.vip.home.mapper.VipHomeMapper;
import com.wsx.ones.vip.home.model.VipHome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by wangshuaixin on 17/1/9.
 */
@Repository
public class VipHomeDaoImpl implements VipHomeDao {

    @Autowired
    private VipHomeMapper vipHomeMapper;

    public int saveVip(VipHome home) {
        return vipHomeMapper.saveVip(home);
    }
}
